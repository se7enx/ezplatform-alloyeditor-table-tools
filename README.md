# eZ Platform AlloyEditor Table Tools

eZ Platform bundle which provides extended UI to manage tables in Online Editor:

- The new cell is selected after the new row/column is inserted [liferay/alloy-editor#1391](https://github.com/liferay/alloy-editor/pull/1391)
- Format text inside table cells [ezsystems/ezplatform-admin-ui#1286](https://github.com/ezsystems/ezplatform-admin-ui/pull/1286)
- Mange embed content/images inside table cells [ezsystems/ezplatform-admin-ui#1034](https://github.com/ezsystems/ezplatform-admin-ui/pull/1034)
- Floating heading/paragraph toolbars
- Custom "Inline Embed" button
- Focus Table/Cell buttons

Merged to eZ Platform:
- [3.0.4](https://github.com/ezsystems/ezplatform/releases/tag/v3.0.4): Manageable links inside table cells [ezsystems/ezplatform-richtext#149](https://github.com/ezsystems/ezplatform-richtext/pull/149)
- [3.1.3](https://github.com/ezsystems/ezplatform/releases/tag/v3.1.3): Fixed custom tag link edit [ezsystems/ezplatform-richtext#163](https://github.com/ezsystems/ezplatform-richtext/pull/163)
- [3.2.0](https://github.com/ezsystems/ezplatform/releases/tag/v3.2.0): Table borders (DocBook schema: [ezsystems/ezplatform-richtext#150](https://github.com/ezsystems/ezplatform-richtext/pull/150), UI: [ezsystems/ezplatform-richtext#151](https://github.com/ezsystems/ezplatform-richtext/pull/151))
- [3.2.3](https://github.com/ezsystems/ezplatform/releases/tag/v3.2.3): Floating table toolbar [ezsystems/ezplatform-richtext#148](https://github.com/ezsystems/ezplatform-richtext/pull/148)

## Installation

1. Require via composer
    ```bash
    composer require contextualcode/ezplatform-alloyeditor-table-tools
    ```

2. Clear browser caches and enjoy!
