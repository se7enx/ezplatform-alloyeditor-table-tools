const path = require('path');
const richTextBase = './../../../../../ezsystems/ezplatform-richtext/src/bundle/Resources/public/';

module.exports = (eZConfig, eZConfigManager) => {
    eZConfigManager.add({
        eZConfig,
        entryName: 'ezplatform-richtext-onlineeditor-js',
        newItems: [
            path.resolve(__dirname, '../public/js/alloyeditor/externals.js'),
            path.resolve(__dirname, '../public/js/alloyeditor/buttons/add-paragraph.js'),
            path.resolve(__dirname, '../public/js/alloyeditor/buttons/focus-table.js'),
            path.resolve(__dirname, '../public/js/alloyeditor/buttons/focus-table-cell.js'),
        ]
    });
    eZConfigManager.replace({
        eZConfig,
        entryName: 'ezplatform-richtext-onlineeditor-js',
        itemToReplace: path.resolve(__dirname, richTextBase + 'js/OnlineEditor/toolbars/config/ez-heading.js'),
        newItem: path.resolve(__dirname, '../public/js/alloyeditor/toolbars/ez-heading.js')
    });
    eZConfigManager.replace({
        eZConfig,
        entryName: 'ezplatform-richtext-onlineeditor-js',
        itemToReplace: path.resolve(__dirname, richTextBase + 'js/OnlineEditor/toolbars/config/ez-paragraph.js'),
        newItem: path.resolve(__dirname, '../public/js/alloyeditor/toolbars/ez-paragraph.js')
    });
    eZConfigManager.replace({
        eZConfig,
        entryName: 'ezplatform-richtext-onlineeditor-js',
        itemToReplace: path.resolve(__dirname, richTextBase + 'js/OnlineEditor/buttons/ez-btn-embedinline.js'),
        newItem: path.resolve(__dirname, '../public/js/alloyeditor/buttons/ez-btn-embedinline.js')
    });
};
