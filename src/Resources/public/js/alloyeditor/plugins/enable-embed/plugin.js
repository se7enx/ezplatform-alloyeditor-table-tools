(function() {
    'use strict';

    if (CKEDITOR.plugins.get('enable_embed')){
        return;
    }

    CKEDITOR.plugins.add('enable_embed', {
        requires: 'ezaddcontent',

        init: function(editor) {
            editor.ezembed.canBeAdded = () => true;
        },
    });
})();