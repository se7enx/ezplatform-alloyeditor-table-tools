import React from 'react';
import AlloyEditor from 'alloyeditor';
import EzButton
    from './../../../../../../../../ezsystems/ezplatform-richtext/src/bundle/Resources/public/js/OnlineEditor/buttons/base/ez-button';

class BtnFocusTableCell extends EzButton {
    static get key() {
        return 'focustablecell';
    }

    focusTableCell(cell) {
        const editor = this.props.editor.get('nativeEditor');
        const selection = editor.getSelection();

        selection.selectElement(cell);

        editor.fire('editorInteraction', {
            nativeEvent: {
                editor: editor,
                target: cell.$,
            },
            selectionData: {
                element: cell,
                region: {}
            }
        });
    }

    render() {
        const parent = this.props.editor.get('nativeEditor').elementPath().lastElement.getParent();
        if (parent.getName() !== 'td' && parent.getName() !== 'th') {
            return null;
        }

        return (
            <button
                className="ae-button ez-btn-ae ez-btn-ae--focus-table-cell"
                onClick={this.focusTableCell.bind(this, parent)}
                tabIndex={this.props.tabIndex}
                title={Translator.trans('focustablecell_btn.label', {}, 'alloy_editor')}
            >
                <svg className="ez-icon ez-btn-ae__icon">
                    <use xlinkHref="/bundles/ezplatformadminui/img/ez-icons.svg#table-cell" />
                </svg>
            </button>
        );
    }
}

AlloyEditor.Buttons[BtnFocusTableCell.key] = AlloyEditor.BtnFocusTableCell = BtnFocusTableCell;