import React from 'react';
import AlloyEditor from 'alloyeditor';
import EzBtnEmbed
    from './../../../../../../../../ezsystems/ezplatform-richtext/src/bundle/Resources/public/js/OnlineEditor/buttons/ez-btn-embed';

export default class EzBtnEmbedInline extends EzBtnEmbed {
    static get key() {
        return 'ezembedinline';
    }

    /**
     * Lifecycle. Renders the UI of the button.
     *
     * @method render
     * @return {Object} The content which should be rendered.
     */
    render() {
        const css = 'ae-button ez-btn-ae ez-btn-ae--embed-inline';
        const label = Translator.trans(/*@Desc("Embed Inline")*/ 'embed_btn_inline.label', {}, 'alloy_editor');

        return (
            <button className={css} onClick={this.chooseContent.bind(this)} tabIndex={this.props.tabIndex} title={label}>
                <svg className="ez-icon ez-btn-ae__icon">
                    <use xlinkHref="/bundles/ezplatformadminui/img/ez-icons.svg#move" />
                </svg>
            </button>
        );
    }
}

AlloyEditor.Buttons[EzBtnEmbedInline.key] = AlloyEditor.EzBtnEmbedInline = EzBtnEmbedInline;

const eZ = (window.eZ = window.eZ || {});

eZ.ezAlloyEditor = eZ.ezAlloyEditor || {};
eZ.ezAlloyEditor.ezBtnEmbedInline = EzBtnEmbedInline;

EzBtnEmbedInline.defaultProps = {
    command: 'ezembedinline',
    modifiesSelection: true,
    udwTitle: Translator.trans(/*@Desc("Select a Content item to embed")*/ 'embed_btn.udw.title', {}, 'alloy_editor'),
    udwContentDiscoveredMethod: 'addEmbed',
    udwConfigName: 'richtext_embed',
};
