import React from 'react';
import AlloyEditor from 'alloyeditor';
import EzButton
    from './../../../../../../../../ezsystems/ezplatform-richtext/src/bundle/Resources/public/js/OnlineEditor/buttons/base/ez-button';

class BtnFocusTable extends EzButton {
    static get key() {
        return 'focustable';
    }

    focusTable() {
        const editor = this.props.editor.get('nativeEditor');
        const element = editor.elementPath().lastElement;
        const table = element.getParents(true).find(e => e.getName() === 'table');
        const selection = editor.getSelection();

        selection.selectElement(table);

        editor.fire('editorInteraction', {
            nativeEvent: {
                editor: editor,
                target: table.$,
            },
            selectionData: {
                element: table,
                region: {}
            }
        });
    }

    render() {
        return (
            <button
                className="ae-button ez-btn-ae ez-btn-ae--focus-table"
                onClick={this.focusTable.bind(this)}
                tabIndex={this.props.tabIndex}
                title={Translator.trans('focustable_btn.label', {}, 'alloy_editor')}
            >
                <svg className="ez-icon ez-btn-ae__icon">
                    <use xlinkHref="/bundles/ezplatformadminui/img/ez-icons.svg#form-data" />
                </svg>
            </button>
        );
    }
}

AlloyEditor.Buttons[BtnFocusTable.key] = AlloyEditor.BtnFocusTable = BtnFocusTable;