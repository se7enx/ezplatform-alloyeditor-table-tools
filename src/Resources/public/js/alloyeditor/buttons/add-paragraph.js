import React from 'react';
import AlloyEditor from 'alloyeditor';
import EzButton
    from './../../../../../../../../ezsystems/ezplatform-richtext/src/bundle/Resources/public/js/OnlineEditor/buttons/base/ez-button';

class BtnAddParagraph extends EzButton {
    static get key() {
        return 'addparagraph';
    }

    insertParagraph() {
        const editor = this.props.editor.get('nativeEditor');
        const element = editor.elementPath().lastElement;
        if (element.getHtml() == '<br>') {
            element.setHtml('');
        }

        // In case if current element is selected, reset its selection
        // Otherwise it will be replaced with a new para
        editor.eZ.moveCaretToElement(editor, element);

        editor.focus();
        const para = editor.document.createElement('p');
        const selection = editor.getSelection();
        const range = selection.getRanges()[0];
        editor.editable().insertElementIntoRange(para, range);
        range.moveToPosition(para, CKEDITOR.POSITION_BEFORE_END);
        selection.selectRanges([range]);

        editor.getSelection().scrollIntoView();
        editor.fire('editorInteraction', {
            nativeEvent: {
                editor: editor,
                target: para.$,
            },
            selectionData: {
                element: para,
                region: editor.getSelectionData().region
            }
        });
    }

    render() {
        return (
            <button
                className="ae-button ez-btn-ae ez-btn-ae--source"
                onClick={this.insertParagraph.bind(this)}
                tabIndex={this.props.tabIndex}
                title={Translator.trans('paragraph_btn.label', {}, 'alloy_editor')}
            >
                <svg className="ez-icon ez-btn-ae__icon">
                    <use xlinkHref="/bundles/ezplatformadminui/img/ez-icons.svg#paragraph-add" />
                </svg>
            </button>
        );
    }
}

AlloyEditor.Buttons[BtnAddParagraph.key] = AlloyEditor.BtnAddParagraph = BtnAddParagraph;